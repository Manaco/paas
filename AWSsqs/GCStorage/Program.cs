﻿using System.IO;
using System.Text;
using Google.Cloud.Storage.V1;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GCStorage
{
    class Program
    {
        static string path = @"C:\Users\Cecropia\Desktop\files-to-check\Log\";
        static string bucketName = "demo-marion";


        static void Main(string[] args)
        {
            var client = StorageClient.Create();

            // CargarArchivos(client, bucketName);
            // DescargandoArchivo(client, bucketName, "test.json");

            var archivosCloud = ListarArchivos(client, bucketName);
            var archivosLocal = Directory.EnumerateFiles(path);

            foreach (var archivoLocal in archivosLocal)
            {
                if (!archivosCloud.Any(x => Path.GetFileName(archivoLocal).Contains(x)))
                {
                    CargarArchivos(client, bucketName, archivoLocal);
                }

            }

            archivosCloud = ListarArchivos(client, bucketName);
            archivosLocal = Directory.EnumerateFiles(path).Select(x => Path.GetFileName(x));

            foreach (var archivoCLoud in archivosCloud)
            {
                if (!archivosLocal.Any(x => archivoCLoud.Contains(x)))
                {
                    DescargandoArchivo(client, bucketName, archivoCLoud, path);
                }
            }
        }



        private static void DescargandoArchivo(StorageClient client, string bucketName, string archivo, string path)
        {
            Console.WriteLine("Bajando archivo {0}", archivo);
            using (var stream = File.OpenWrite(string.Concat(path, archivo)))
            {
                client.DownloadObject(bucketName, archivo, stream);
            }

        }

        private static void CargarArchivos(StorageClient client, string bucketName, string file)
        {
            Console.WriteLine("Uploading {0}", Path.GetFileName(file));
            var obj1 = client.UploadObject(bucketName, Path.GetFileName(file), "text/plain", File.OpenRead(file));
        }



        public static List<string> ListarArchivos(StorageClient client, string bucket)
        {
            var result = new List<string>();
            foreach (var item in client.ListObjects(bucket, ""))
            {
                result.Add(item.Name);
                Console.WriteLine(item.Name);
            }

            return result;

        }
    }
}
