﻿using System;
using Amazon;
using Amazon.Runtime;
using Amazon.SimpleWorkflow;
using Amazon.SimpleWorkflow.Model;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;

namespace AmasonSWF
{
    class Program
    {
        private static string domainName = "ManuDomain";
        private static IAmazonSimpleWorkflow _swfClient;

        static void Main(string[] args)
        {

            const string workflowName = "ManulWorkFlow";

            /// Amaazon Client
            var awsCred = new BasicAWSCredentials("AKIAJFJJ36E32XTVULXQ", "bvskCivdB3j1T/r6o/0KSXFaq+BTgkX6YolAn0tD");
            _swfClient = new AmazonSimpleWorkflowClient(awsCred, Amazon.RegionEndpoint.USEast2);

            //Setup
            RegisterDomain();
            RegisterActivity("Activity1A", "Activity1");
            RegisterActivity("Activity1B", "Activity2");
            RegisterActivity("Activity2", "Activity2");

            RegisterWorkflow(workflowName);

            // Launch workers to service Activity1A and Activity1B
            // This is achieved by sharing same tasklist name (i.e.) "Activity1"

            Task.Run(() => Worker("Activity1"));
            Task.Run(() => Worker("Activity1"));

            //Launch workers for activity2
            Task.Run(() => Worker("Activity2"));
            Task.Run(() => Worker("Activity2"));

            // Start the deciders, which defines the structure / flow of workflow
            Task.Run(() => Decider());

            Task.Run(() => StartWorkflow(workflowName));








        }

        private static void StartWorkflow(string name)
        {

            /// Amaazon Client
            var awsCred = new BasicAWSCredentials("AKIAJFJJ36E32XTVULXQ", "bvskCivdB3j1T/r6o/0KSXFaq+BTgkX6YolAn0tD");
            _swfClient = new AmazonSimpleWorkflowClient(awsCred, Amazon.RegionEndpoint.USEast2);

            var workFlowID = "Hello World WorkFlowId - " + DateTime.Now.Ticks;
            _swfClient.StartWorkflowExecution(new StartWorkflowExecutionRequest()
            {
                Input = "{\"inputparam1\" : \"value1\"}", // Serialize input to a string
                WorkflowId = workFlowID,
                Domain = domainName,
                WorkflowType = new WorkflowType
                {
                    Name = name,
                    Version = "2.0"
                }
            });

            Console.WriteLine("Setup: Workflow Instance created ID=" + workFlowID);

        }



        private static void Decider()
        {

            int activityCount = 0; // this refers to total of activities  per workflow
            var awsCred = new BasicAWSCredentials("AKIAJFJJ36E32XTVULXQ", "bvskCivdB3j1T/r6o/0KSXFaq+BTgkX6YolAn0tD");
            _swfClient = new AmazonSimpleWorkflowClient(awsCred, Amazon.RegionEndpoint.USEast2);


            while (true)
            {
                Console.WriteLine("Decider: Pooling for desicion task ...");
                PollForDecisionTaskRequest request = new PollForDecisionTaskRequest
                {
                    Domain =  domainName,
                    TaskList = new TaskList { Name = "HelloWorld"}
                };

                PollForDecisionTaskResponse response = _swfClient.PollForDecisionTask(request);
                if(response.DecisionTask.TaskToken == null)
                {
                    Console.WriteLine("Decider: Null");
                    continue;
                }

                int completedActivityTaskCount = 0, totalActivityTaskCount = 0;
                foreach (HistoryEvent e in response.DecisionTask.Events)
                {
                    Console.WriteLine("Decider:  eventype - " + e.EventType + ", EventId - " + e.EventId);

                    if(e.EventType == "ActivityTaskCompleted")
                    {
                        completedActivityTaskCount++;

                    }
                    if (e.EventType.Value.StartsWith("Activity"))
                    {
                        totalActivityTaskCount++;
                    }
                    Console.WriteLine("... completedCount" +  completedActivityTaskCount);

                    var decisions = new List<Decision>();
                    if (totalActivityTaskCount == 0)
                    {
                        ScheduleActivity("Activity1A", decisions);
                        ScheduleActivity("Activity1B", decisions);
                        ScheduleActivity("Activity2", decisions);
                        ScheduleActivity("Activity2", decisions);
                        activityCount = 4;
                    } else if(completedActivityTaskCount == activityCount)
                    {
                        var desicion = new Decision {
                            DecisionType = DecisionType.CompleteWorkflowExecution,
                            CompleteWorkflowExecutionDecisionAttributes = new CompleteWorkflowExecutionDecisionAttributes
                            {
                                Result = "{\"Result\":\"WF Completely\"}"
                            }                                
                        };

                        decisions.Add(desicion);
                        Console.WriteLine("Decider: WorkFlow Complete!!!");
                    }

                    var respondeDesicionTaskCompletedRequest = new RespondDecisionTaskCompletedRequest
                    {
                        Decisions = decisions,
                        TaskToken = response.DecisionTask.TaskToken
                    };

                    _swfClient.RespondDecisionTaskCompleted(respondeDesicionTaskCompletedRequest);
                }
            }

        }

        private static void Worker(string taskListName)
        {

            var prefix = $"Worker {taskListName} : {System.Threading.Thread.CurrentThread.ManagedThreadId:x}";

            while (true)
            {
                Console.WriteLine(prefix + ": Polling for activity task ...");
                PollForActivityTaskRequest poolForActivityTaskRequest = new PollForActivityTaskRequest
                {
                    Domain = domainName,
                    TaskList = new TaskList
                    {
                        // Pool only task assigned to me
                        Name = taskListName
                    }
                };

                var PollForActivityResp = _swfClient.PollForActivityTask(poolForActivityTaskRequest);
                var respondActivityTaskCompletedRequest = new RespondActivityTaskCompletedRequest
                {
                    Result = "{\"activityResult1\":\"Result Value\"}",
                    TaskToken = PollForActivityResp.ActivityTask.TaskToken
                };

                if(PollForActivityResp.ActivityTask.ActivityId == null)
                {
                    Console.WriteLine(prefix + ": NULL");
                }
                else
                {
                    _swfClient.RespondActivityTaskCompleted(respondActivityTaskCompletedRequest);
                    Console.WriteLine(prefix + ": Activity task completed. ActivityId - " + PollForActivityResp.ActivityTask.ActivityId);
                }

            }
        }

        private static void RegisterWorkflow(string name)
        {

            var listWorkflowRequest = new ListWorkflowTypesRequest
            {
                Name = name,
                Domain = domainName,
                RegistrationStatus = RegistrationStatus.REGISTERED
            };


            if (_swfClient.ListWorkflowTypes(listWorkflowRequest).WorkflowTypeInfos.TypeInfos.FirstOrDefault(x => x.WorkflowType.Version == "2.0") == null)
            {

                var request = new RegisterWorkflowTypeRequest
                {
                    DefaultChildPolicy = ChildPolicy.TERMINATE,
                    DefaultExecutionStartToCloseTimeout = "300",
                    DefaultTaskList = new TaskList()
                    {
                        Name = "HelloWorld"//Decider needs to pool for this task
                    },
                    DefaultTaskStartToCloseTimeout = "150",
                    Domain = domainName,
                    Name = name,
                    Version = "2.0"
                };

                _swfClient.RegisterWorkflowType(request);
                Console.WriteLine("Setup: Registered Workflow Name - " + request.Name);

            }

        }

        private static void RegisterActivity(string name, string tasklistName)
        {
            // Register activity if this is not already registerd
            var listActivityRequest = new ListActivityTypesRequest
            {
                Domain = domainName,
                Name = name,
                RegistrationStatus = RegistrationStatus.REGISTERED
            };

            if (_swfClient.ListActivityTypes(listActivityRequest).ActivityTypeInfos.TypeInfos.FirstOrDefault(x => x.ActivityType.Version == "2.0") == null)
            {
                var request = new RegisterActivityTypeRequest
                {
                    Name = name,
                    Domain = domainName,
                    Description = "Hello World Activities",
                    Version = "2.0",
                    DefaultTaskList = new TaskList { Name = tasklistName },//worker poll based on this
                    DefaultTaskScheduleToCloseTimeout = "3600",
                    DefaultTaskScheduleToStartTimeout = "150",
                    DefaultTaskStartToCloseTimeout = "450",
                    DefaultTaskHeartbeatTimeout = "NONE"
                };

                _swfClient.RegisterActivityType(request);
                Console.WriteLine("Setup: Created Activity Name - " + request.Name);
            }

        }

        private static void RegisterDomain()
        {
            //Register if the domain is not already registered
            var listDomainRequest = new ListDomainsRequest()
            {
                RegistrationStatus = RegistrationStatus.REGISTERED
            };

            if (_swfClient.ListDomains(listDomainRequest).DomainInfos.Infos.FirstOrDefault(x => x.Name == domainName) == null)
            {
                var request = new RegisterDomainRequest()
                {
                    Name = domainName,
                    Description = "Manu First Domain Description",
                    WorkflowExecutionRetentionPeriodInDays = "1"
                };

                Console.WriteLine("Setup: Created Domain - " + domainName);
                _swfClient.RegisterDomain(request);
            }
        }


        static void ScheduleActivity(string name, ICollection<Decision> decisions)
        {
            Decision decision = new Decision
            {
                DecisionType = DecisionType.ScheduleActivityTask,
                ScheduleActivityTaskDecisionAttributes = //Uses DefaultTaskList
                new ScheduleActivityTaskDecisionAttributes
                {
                    ActivityType = new ActivityType
                    {
                        Name = name,
                        Version = "2.0"
                    },
                    ActivityId = name + "- " + System.Guid.NewGuid().ToString(),
                    Input = "{\"activityInput\": \"value1\"}"
                }
            };

            Console.WriteLine("Decider: Activity=" +
                decision.ScheduleActivityTaskDecisionAttributes.ActivityId);

            decisions.Add(decision);
        }
    }
}
