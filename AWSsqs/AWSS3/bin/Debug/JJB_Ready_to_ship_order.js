/**
 * @param {Object}
 *            datain Parameter object, contains the order information
 * @returns {Object} The updated record or error message.
 */
function save_ready_to_ship_order(datain) {

	var ordersArray =  datain['orders'];

	//All orders loop.
	for ( i = 0; i < ordersArray.length; i++) { 

		//Get order by NetSuite internalID.
		record = nlapiLoadRecord('salesorder', ordersArray[i].orderID);

		//Set field values to record only if exist

		if(  ordersArray[i].memo != null ){
			record.setFieldValue('memo',  ordersArray[i].memo);
		}

		if(  ordersArray[i].shipMethod != null ){
			record.setFieldValue('shipmethod',  ordersArray[i].shipMethod);
		}

		nlapiSubmitRecord(record, true);

	}
	return record;
}

