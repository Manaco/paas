﻿using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.IO;

namespace AWSS3
{
    class Program
    {
        static string bucketName = "demo-manu";
        //static string filepath = "JJB_Ready_to_ship_order.js";
        static IAmazonS3 _cient;

        static void Main(string[] args)
        {
            /// Amaazon Client
            var awsCred = new BasicAWSCredentials("AKIAJFJJ36E32XTVULXQ", "bvskCivdB3j1T/r6o/0KSXFaq+BTgkX6YolAn0tD");
            _cient = new AmazonS3Client(awsCred, Amazon.RegionEndpoint.USEast1);

            /// Azure Block Blob
            var connStringStorage = "DefaultEndpointsProtocol=https;AccountName=demomanu;AccountKey=0G2AJZTXcX8sAjLJwbDwMoDUuw5Czufzx9PJ2gPMYIGjDph9aycQwSudjJNL/JcdSpwfiTD0J+cf1v083mLNeg==;EndpointSuffix=core.windows.net";
            var storageAccount = CloudStorageAccount.Parse(connStringStorage);
            var blobClient = storageAccount.CreateCloudBlobClient();
            var container = blobClient.GetContainerReference("containermanu");


            // Process the list of files found in the directory.
            string[] fileEntries = Directory.GetFiles(@"C:\Users\Cecropia\Desktop\files-to-check\CFM\");


            using (_cient)
            {
                foreach (var item in fileEntries)
                {

                    //ListingObjects(_cient);
                    Console.WriteLine("Uploading and Object");
                    WritingObject(_cient, item, Path.GetFileName(item));

                    //UploadAzureBlob(container.GetBlockBlobReference(Path.GetFileName(item)), item);
                    //Console.WriteLine("Press any key to continue");
                }


            }




            //using (_cient)
            //{

            //    ListingObjects(_cient);

            //    Console.WriteLine("Uploading and Object");
            //    WritingObject(_cient);

            //    Console.WriteLine("Press any key to continue");
            //    Console.ReadKey();


            //    Console.WriteLine("Obteniendo (GET) un objeto");
            //    string data = ReadObjectData(_cient);
            //    Console.WriteLine("Objeto Retornado: {0}: ", data);
            //    Console.ReadKey();


            //}





        }


        private static void WritingObject(IAmazonS3 aClient, string filePath, string keyName)
        {
            try
            {
                PutObjectRequest putRequest = new PutObjectRequest
                {
                    BucketName = bucketName,
                    Key = "hola.txt",
                    FilePath = filePath,
                    ContentType = "text/plain",
                };
                var response1 = aClient.PutObject(putRequest);

                //2 put object, set content type and add metadata
               /* PutObjectRequest putObjectRequest2 = new PutObjectRequest
                {
                    BucketName = bucketName,
                    Key = "hola.txt",
                    FilePath = filePath,
                    ContentType = "text/plain",
                };*/
                /*
                putObjectRequest2.Metadata.Add("x-amz-meta-title", filePath);
                var response2 = aClient.PutObject(putObjectRequest2);*/

                Console.WriteLine("{0} subido a amazon", filePath);
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null && (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId"))
                    || amazonS3Exception.ErrorCode.Equals("InvalidSecurity"))
                {
                    Console.WriteLine("Check the provided AWS credentials");
                    Console.WriteLine("For Service sign up go to aws.amazon.com");
                }
                else
                {
                    Console.WriteLine("Error Occurred. Message: '{0}' when writting and object", amazonS3Exception.Message);
                }
            }
        }



        public static void UploadAzureBlob(CloudBlockBlob blockBob, string file)
        {

            using (var fileStream = File.OpenRead(file))
            {
                blockBob.UploadFromStream(fileStream);

            }
            Console.WriteLine("{0} subida a Azure", Path.GetFileName(file));


        }


        //static string ReadObjectData(IAmazonS3 pClient)
        //{

        //    var responseBody = "";
        //    var request = new GetObjectRequest()
        //    {
        //        BucketName = bucketName,
        //        Key = keyname
        //    };

        //    using (var response = pClient.GetObject(request))
        //    using (var responseStream = response.ResponseStream)
        //    using (var reader = new StreamReader(responseStream))
        //    {
        //        string title = response.Metadata["x-amz-meta-title"];
        //        Console.WriteLine("the object title is {0} ", title);

        //        responseBody = reader.ReadToEnd();
        //    }

        //    return responseBody;
        //}


        public static void ListingObjects(IAmazonS3 pClient)
        {
            try
            {
                var request = new ListObjectsV2Request
                {
                    BucketName = bucketName,
                    MaxKeys = 10
                };

                ListObjectsV2Response response;
                do
                {
                    response = pClient.ListObjectsV2(request);
                    foreach (var entry in response.S3Objects)
                    {
                        Console.WriteLine("Key - {0} size - {1} ", entry.Key, entry.Size);

                    }
                    request.ContinuationToken = response.NextContinuationToken;
                } while (response.IsTruncated);
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null && (amazonS3Exception.ErrorCode.Equals("invalidAccessKey")) ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity"))
                {
                    Console.WriteLine("verificar credenciales de aws");
                }
                else
                {
                    Console.WriteLine("Error. message: '{0}' listando objetos", amazonS3Exception.Message);
                }

            }
        }
    }
}
