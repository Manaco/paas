﻿using System;
using System.Collections.Generic;
using Google.Cloud.PubSub.V1;
using Google.Protobuf;
using Grpc.Core;

namespace GoogleC
{
    class Program
    {
        private const string ProjectId = "arctic-inkwell-184622";
        private const string TopicId = "cecropia-demp";
        private const string SuscriptIonId = "cecropia-sub";

        static void Main(string[] args)
        {
            ListTopics();
            PublishTopTopic();

            var response = ShowMessagesForSubscription();
            while (response != null && response.ReceivedMessages.Count > 0)
            {
                response = ShowMessagesForSubscription();
            }


        }

        private static PullResponse ShowMessagesForSubscription()
        {

            var subscriptionName = new SubscriptionName(ProjectId, SuscriptIonId);
            var subscription = SubscriberClient.Create();

            try
            {
                var response = subscription.Pull(subscriptionName, true, 10);
                var all = response.ReceivedMessages;

                foreach (ReceivedMessage message in all)
                {
                    string id = message.Message.MessageId;
                    string publishDate = message.Message.PublishTime.ToDateTime().ToString("dd-MM-yyy HH:MM:ss");
                    string data = message.Message.Data.ToStringUtf8();

                    Console.WriteLine($"{0} {publishDate} - {data}");
                    Console.WriteLine(" Acknoweldging...");
                    subscription.Acknowledge(subscriptionName, new string[] { message.AckId });
                    Console.WriteLine("done");
                }

                return response;
            }
            catch (RpcException ex)
            {
                Console.WriteLine("Something went wrong {0}", ex.Message);
                return null;
            }


        }

      

        private static void PublishToTopic()
        {
            throw new NotImplementedException();
        }

        private static void PublishTopTopic()
        {
            var topicName = new TopicName(ProjectId, TopicId);
            var publisher = PublisherClient.Create();

            // Create a message
            var message = new PubsubMessage
            {
                Data = ByteString.CopyFromUtf8("hello world")
            };

            message.Attributes.Add("Id", "1");

            // Add it to the list of messages to publish
            var messageList = new List<PubsubMessage> { message };


            //Publish it
            Console.WriteLine("Publishing...");

            var response = publisher.Publish(topicName, messageList);

            //get the message id GCloud gave us
            Console.WriteLine(" Message ids published");
            foreach (var messageId in response.MessageIds)
            {
                Console.WriteLine($" {messageId}");
            }

        }

        private static void ListTopics()
        {
            // List all topics of the project
            var publisher = PublisherClient.Create();
            var topics = publisher.ListTopics(new ProjectName(ProjectId));

            foreach (var item in topics)
            {
                Console.WriteLine(item.Name);
            }
        }
    }
}
