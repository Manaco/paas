//https://cloud.google.com/storage/docs/object-basics#storage-upload-object-nodejs
// Imports the Google Cloud client library
const Storage = require('@google-cloud/storage');

// Creates a client
const storage = new Storage();

/**
 * TODO(developer): Uncomment the following lines before running the sample.
 */
const bucketName = 'demo-marion';
const filename = 'package.json';

// Uploads a local file to the bucket
storage
    .bucket(bucketName)
    .upload(filename)
    .then(() => {
        console.log(`${filename} uploaded to ${bucketName}.`);
    })
    .catch(err => {
        console.error('ERROR:', err);
    });